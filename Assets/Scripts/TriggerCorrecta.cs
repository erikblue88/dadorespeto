﻿using UnityEngine;

public class TriggerCorrecta : MonoBehaviour
{
    public bool disponible = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.name);
        collision.GetComponent<Accion>().isInCollision = true;
        if (disponible)
        {
            if(collision.GetComponent<Accion>() != null)
            {
                if(collision.GetComponent<Accion>().correcta)
                {
                    collision.GetComponent<MouseDragBehavior>().dejarAqui = true;
                    collision.GetComponent<MouseDragBehavior>().parent = transform;
                    disponible = false;
                }
                else
                    collision.GetComponent<MouseDragBehavior>().idxTemaError = collision.GetComponent<Accion>().idxTipo;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.GetComponent<Accion>().isInCollision = false;
        if (collision.GetComponent<Accion>() != null && collision.GetComponent<Accion>().correcta)
        {
            collision.GetComponent<MouseDragBehavior>().dejarAqui = false;
            collision.GetComponent<MouseDragBehavior>().parent = null;
            disponible = true;
        }
    }
}
