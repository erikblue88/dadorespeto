﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class ManagerReactivos : Singleton<ManagerReactivos>
{
    public List<Reactivos> reactivos;
    public List<Text> textosDesordenados;
    public List<Sprite> fondos;
    public List<Sprite> icons;
    public List<Sprite> finales;
    public Image fondo;
    public Image icon;
    public Image final;
    public Text textIndexTema;
    public Text textTemaSeleccionado;
    public Text textTemaSeleccionadoJuego;
    public Text textTemaSeleccionadoError;
    public GameObject panelTitleTemaSeleccionado;
    public GameObject panelJuego;
    public GameObject panelCorrecta;
    public GameObject panelFinal;
    public GameObject panelError;
    public GameObject mainBG;
    public int aciertos;
    public Animator animDado;
    public AudioControl ac;

    [SerializeField] private GameObject _panelIntro;

    int tema;
    bool dadoTirado;

    public void TirarDado()
    {
        if (dadoTirado)
            return;
        dadoTirado = true;
        StartCoroutine(tiraDado());
    }

    public void ArmaPartida()
    {
        List<Text> tempDesordenados = textosDesordenados;
        List<string> tempReglasCorrecto = reactivos[tema].reglas;
        for (int x = 0; x < 3; x++)
        {
            int idxtexto = Random.Range(0, tempDesordenados.Count);
            int rdmtxtCorecto = Random.Range(0, tempReglasCorrecto.Count);
            tempDesordenados[idxtexto].text = tempReglasCorrecto[rdmtxtCorecto];
            tempDesordenados[idxtexto].GetComponent<Accion>().correcta = true;
            tempDesordenados[idxtexto].GetComponent<Accion>().idxTipo = tempDesordenados[idxtexto].GetComponent<MouseDragBehavior>().idxTemaError = tema;
            tempDesordenados.RemoveAt(idxtexto);
            tempReglasCorrecto.RemoveAt(rdmtxtCorecto);
        }
        for (int x = 3; x < 12; x++)
        {
            int idxtexto = Random.Range(0, tempDesordenados.Count);
            int rdmtema;
            do
            {
                rdmtema = Random.Range(0, 6);
            } while (rdmtema == tema);
            int rdmReactivo = Random.Range(0, reactivos[rdmtema].reglas.Count);
            tempDesordenados[idxtexto].text = reactivos[rdmtema].reglas[rdmReactivo];
            tempDesordenados[idxtexto].GetComponent<Accion>().correcta = false;
            tempDesordenados[idxtexto].GetComponent<Accion>().idxTipo = tempDesordenados[idxtexto].GetComponent<MouseDragBehavior>().idxTemaError = rdmtema;
            tempDesordenados.RemoveAt(idxtexto);
            reactivos[rdmtema].reglas.RemoveAt(rdmReactivo);
        }
    }

    public void Acierto()
    {
        aciertos++;
        if (aciertos == 3)
        {
            panelFinal.SetActive(true);
            ac.PlayAudio(2);
        }
        else
            StartCoroutine(correcta());
    }

    public void Error(int idxTemaError)
    {
        StartCoroutine(error(idxTemaError));
    }

    public void Reiniciar()
    {
        SceneManager.LoadScene(0);
    }

    public void PasaIntro()
    {
        _panelIntro.SetActive(false);
        animDado.gameObject.SetActive(true);
    }

    IEnumerator correcta()
    {
        panelCorrecta.SetActive(true);
        ac.PlayAudio(0);
        yield return new WaitForSeconds(2);
        panelCorrecta.SetActive(false);
    }

    IEnumerator error(int idxtemaError)
    {
        textTemaSeleccionadoError.text = reactivos[idxtemaError].tipo;
        panelError.SetActive(true);
        ac.PlayAudio(1);
        yield return new WaitForSeconds(2);
        panelError.SetActive(false);
    }

    IEnumerator tiraDado()
    {
        tema = Random.Range(0, 6);
        textIndexTema.text = tema.ToString();
        switch (tema)
        {
            case 0:
                animDado.Play("sprites 1");
                break;
            case 1:
                animDado.Play("sprites 2");
                break;
            case 2:
                animDado.Play("sprites 3");
                break;
            case 3:
                animDado.Play("sprites 4");
                break;
            case 4:
                animDado.Play("sprites 5");
                break;
            case 5:
                animDado.Play("sprites 6");
                break;
        }
        yield return new WaitForSeconds(2f);
        textTemaSeleccionado.text = textTemaSeleccionadoJuego.text = reactivos[tema].tipo; 
        yield return new WaitForSeconds(1);
        panelTitleTemaSeleccionado.SetActive(true);
        yield return new WaitForSeconds(1f);
        ArmaPartida();
        fondo.sprite = fondos[tema];
        icon.sprite = icons[tema];
        final.sprite = finales[tema];
        yield return new WaitForSeconds(2f);
        panelJuego.SetActive(true);
        panelTitleTemaSeleccionado.SetActive(false);
    }
}

[System.Serializable]
public class Reactivos
{
    public string tipo;
    public List<string> reglas; 
}
